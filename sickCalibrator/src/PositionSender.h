#include "ofMain.h"
#include "ofxOsc.h"

//#define HOST "localhost"
#define HOST "192.168.0.11"
#define PORT 1234

class PositionSender{
    
    
public:
    
    PositionSender();
   ~PositionSender(){};
    
    void draw(vector <ofVec2f> pos, float trackingRegionx,float trackingRegiony,float trackingRegionW,float trackingRegionH);
    ofxOscSender sender;
};
