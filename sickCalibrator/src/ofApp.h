#pragma once

#include "ofMain.h"
#include "ofxSick.h"
#include "ofxSickTracker.h"
#include "ofxCv.h"
#include "PositionSender.h"
#include "ofxGui.h"

#define WIDTH 1024
#define HEIGHT 768

#define  MAX_HANDS 200

class ofApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void trackingRegionDebug();
    
    ofxPanel gui;
    ofxFloatSlider angleOffset;
    ofxFloatSlider trackingRegionX;
    ofxFloatSlider trackingRegionY;
    ofxFloatSlider trackingRegionW;
    ofxFloatSlider trackingRegionH;
    ofxFloatSlider viewerScale;
    ofxFloatSlider worldX;
    ofxFloatSlider worldY;
    ofxFloatSlider xOffset;
    ofxFloatSlider yOffset;
    ofxSickGrabber grabber;
    ofxSickPlayer player;
    ofxSick* sick;
    bool recording;

    ofxSickTracker<ofxSickFollower> tracker;
    ofRectangle trackingRegion;
    
    //POSITION SENDER
    PositionSender  positionSender;
    int globalOffset;
    vector<ofVec2f> pos;

    
};
