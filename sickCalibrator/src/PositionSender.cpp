#include "PositionSender.h"



PositionSender::PositionSender(){
   
    //OSC SENDER
    sender.setup(HOST, PORT);
    
}




void PositionSender::draw(vector <ofVec2f> pos, float trackingRegionx,float trackingRegiony,float trackingRegionW,float trackingRegionH){
    
    vector <ofVec2f> blobs;
    //blobs.clear();
    
    for(int i=0; i<pos.size(); i++){
        
        blobs.push_back(pos[i]);
        blobs[i].x = ofMap( blobs[i].x, trackingRegionx, trackingRegionW+trackingRegionx,0.0,1.0);
        blobs[i].y = ofMap( blobs[i].y, trackingRegiony, trackingRegionH+trackingRegiony,0.0,1.0);
        ofDrawBitmapStringHighlight(ofToString(blobs[i].x), blobs[i].x, blobs[i].y);
        ofDrawBitmapStringHighlight(ofToString(blobs[i].y), blobs[i].x, blobs[i].y+40);
        ofDrawCircle(blobs[i].x, blobs[i].y ,100,100);
        
        ofxOscMessage mx;
        mx.setAddress("/contact/add");
        mx.addIntArg(i);
        mx.addFloatArg( blobs[i].x );
        mx.addFloatArg( blobs[i].y );
        sender.sendMessage(mx);
        
        
    }
    
}














