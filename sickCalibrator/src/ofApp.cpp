#include "ofApp.h"
using namespace cv;
using namespace ofxCv;
//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetVerticalSync(true);
    ofSetCircleResolution(64);
    ofSetLogLevel(OF_LOG_VERBOSE);
    ofSetFrameRate(60);
    
    //GUI
    gui.setup();
    gui.add(trackingRegionX.setup("tracking region x",0,0,-3000));
    gui.add(trackingRegionY.setup("tracking region y",0,-ofGetHeight()*6,ofGetHeight()*6));
    gui.add(trackingRegionW.setup("tracking region w",0,0,ofGetWidth()*6));
    gui.add(trackingRegionH.setup("tracking region h",0,0,ofGetHeight()*10));
    gui.add(viewerScale.setup("viewer scale",0,0,0.2));
    gui.add(worldX.setup("world x",0,-ofGetWidth()*4,ofGetWidth()*9));
    gui.add(worldY.setup("world y",0,-ofGetHeight()*4,ofGetHeight()*9));
    gui.add(angleOffset.setup("angle offset",0,-4,4));
    gui.add(xOffset.setup("offset x",0,-3000,3000));
    gui.add(yOffset.setup("offset y",0,-3000,3000));


    gui.loadFromFile("settings.xml");
    
    //SICK STUFF
    //recording data
    recording = false;
    grabber.setup();
    //player.load("penal.lms");
    sick = &grabber;
    trackingRegion.set(trackingRegionX, trackingRegionY,trackingRegionW, trackingRegionH);
    //tracker.setupKmeans(5, 1);
    tracker.setupNaive(1, 10);
    //tracker.setMaximumDistance(100);
    //tracker.setPersistence(10);
    tracker.setRegion(trackingRegion);

    
    
    
}

//--------------------------------------------------------------
void ofApp::update(){
    sick->update();
    sick->setAngleOffset(angleOffset);

    if(sick->isFrameNew()) {
        tracker.update(*sick);
    }
    
    
   // positionSender.update();
    
    pos.clear();
    for (int i=0; i<tracker.size(); i++) {
        ofVec2f posScaled;
        posScaled.x =   tracker.getCluster(i).x+xOffset;
        posScaled.y =   tracker.getCluster(i).y+yOffset;
        pos.push_back(posScaled);
        
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    

    
    ofBackground(0);
    
    ofPushMatrix();
    
    ofTranslate(worldX, worldY);
    float scale = ofMap(viewerScale, 0, 1, 0.0, 2.0, true);
    
    ofPushMatrix();
    ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);
    ofScale(scale, scale);
    sick->draw(10, 5000);
    tracker.draw();
    positionSender.draw(pos, trackingRegionX,trackingRegionY, trackingRegionW,trackingRegionH);
    
    ofSetColor(255, 0, 0);
    trackingRegionDebug();
    ofPopMatrix();
  
    ofPopMatrix();
    
    ofPushMatrix();
    gui.draw();
    ofDrawBitmapStringHighlight("FPS:"+ofToString(ofGetFrameRate()), 50,ofGetHeight()- 50);
    ofDrawBitmapString(ofToString(tracker.size()) + " clusters", 14, 14);
    ofPopMatrix();
    
    
}
void ofApp::trackingRegionDebug(){
    
    ofFill();
    ofSetColor(255,0,0,140);
    ofDrawRectangle(trackingRegionX, trackingRegionY, trackingRegionW, trackingRegionH);
    
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    if(key == 's'){
        gui.saveToFile("settings.xml");
        
    }
    
    if(key=='f'){
        
        ofToggleFullscreen();
    }
    
    if(key == 'r') {
        recording = !recording;
        if(recording) {
            grabber.startRecording();
        } else {
            grabber.stopRecording("catchMe.lms");
        }
    }
    if(key == '\t') {
        if(sick == &player) {
            sick = &grabber;
        } else {
            sick = &player;
        }
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
    
}
